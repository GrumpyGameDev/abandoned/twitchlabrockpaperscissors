# TwitchLabRockPaperScissors

A project in which an application logs into Twitch and listens for chat and plays the game Rock, Paper, Scissors.

What to put in yer appSettings.config file:

```
<appSettings>
  <add key="twitch_username" value="<yer twitch user name>" />
  <add key="access_token" value="<yer access token, go to https://twitchapps.com/tmi/>" />
  <add key="channel" value="<yer channel>" />
</appSettings>
```