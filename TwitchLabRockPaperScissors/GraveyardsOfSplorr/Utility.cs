﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public static class Utility
    {
        private static Dictionary<int, int> _skullDie = new Dictionary<int, int>()
        {
            [0] = 1,
            [1] = 1
        };
        private static Dictionary<int, int> _whiteShieldDie = new Dictionary<int, int>()
        {
            [0] = 2,
            [1] = 1
        };
        private static Dictionary<int, int> _blackShieldDie = new Dictionary<int, int>()
        {
            [0] = 5,
            [1] = 1
        };
        private static Dictionary<int, int> _emptyDie = new Dictionary<int, int>()
        {
            [0] = 1
        };
        public static Dictionary<int,int> GetSkullDice(int count)
        {
            if(count<1)
            {
                return _emptyDie;
            }
            else
            {
                count--;
                Dictionary<int, int> result = _skullDie;
                while(count>0)
                {
                    count--;
                    result = result.Combine(_skullDie);
                }
                return result;
            }
        }
        public static Dictionary<int, int> GetWhiteShieldDice(int count)
        {
            if (count < 1)
            {
                return _emptyDie;
            }
            else
            {
                count--;
                Dictionary<int, int> result = _whiteShieldDie;
                while (count > 0)
                {
                    count--;
                    result = result.Combine(_whiteShieldDie);
                }
                return result;
            }
        }
        public static Dictionary<int, int> GetBlackShieldDice(int count)
        {
            if (count < 1)
            {
                return _emptyDie;
            }
            else
            {
                count--;
                Dictionary<int, int> result = _blackShieldDie;
                while (count > 0)
                {
                    count--;
                    result = result.Combine(_blackShieldDie);
                }
                return result;
            }
        }
        public static int FloorModulus(int value, int divisor)
        {
            var result = value % divisor;
            if(result<0)
            {
                result += divisor;
            }
            return result;
        }
    }
}
