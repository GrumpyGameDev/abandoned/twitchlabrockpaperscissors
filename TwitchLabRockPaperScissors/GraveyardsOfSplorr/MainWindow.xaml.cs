﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraveyardsOfSplorr
{
    public partial class MainWindow : Window
    {
        private Bot _bot;

        private World _world = new World();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Up)
            {
                _world.MoveAvatar(MoveDirection.North);
                UpdateDisplay();
            }
            else if (e.Key == Key.Down)
            {
                _world.MoveAvatar(MoveDirection.South);
                UpdateDisplay();
            }
            else if (e.Key == Key.Left)
            {
                _world.MoveAvatar(MoveDirection.West);
                UpdateDisplay();
            }
            else if (e.Key == Key.Right)
            {
                _world.MoveAvatar(MoveDirection.East);
                UpdateDisplay();
            }
        }

        internal void HandleCommand(string username, string token)
        {
            if (token=="!up")
            {
                AddMessage("Twitch Moves Up!");
                _world.MoveAvatar(MoveDirection.North);
                UpdateDisplay();
            }
            else if (token == "!down")
            {
                AddMessage("Twitch Moves Down!");
                _world.MoveAvatar(MoveDirection.South);
                UpdateDisplay();
            }
            else if (token == "!left")
            {
                AddMessage("Twitch Moves Left!");
                _world.MoveAvatar(MoveDirection.West);
                UpdateDisplay();
            }
            else if (token == "!right")
            {
                AddMessage("Twitch Moves Right!");
                _world.MoveAvatar(MoveDirection.East);
                UpdateDisplay();
            }
            else if (token == "!restart")
            {
                if(_world.AvatarDead)
                {
                    _world = new World();
                }
                UpdateDisplay();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateDisplay();
            _bot = new Bot(this);
        }

        internal void AddMessage(string message)
        {
            Dispatcher.Invoke(() => 
            {
                ((TextBlock)FindName("Message0")).Text = ((TextBlock)FindName("Message1")).Text;
                ((TextBlock)FindName("Message1")).Text = ((TextBlock)FindName("Message2")).Text;
                ((TextBlock)FindName("Message2")).Text = ((TextBlock)FindName("Message3")).Text;
                ((TextBlock)FindName("Message3")).Text = ((TextBlock)FindName("Message4")).Text;
                ((TextBlock)FindName("Message4")).Text = ((TextBlock)FindName("Message5")).Text;
                ((TextBlock)FindName("Message5")).Text = ((TextBlock)FindName("Message6")).Text;
                ((TextBlock)FindName("Message6")).Text = ((TextBlock)FindName("Message7")).Text;
                ((TextBlock)FindName("Message7")).Text = message;
            });
        }

        private void UpdateDisplay()
        {
            UpdateMap();
            UpdateStats();
        }

        private void UpdateMap()
        {
            for (int screenColumn = 0; screenColumn < Constants.ScreenColumns; ++screenColumn)
            {
                for (int screenRow = 0; screenRow < Constants.ScreenRows; ++screenRow)
                {
                    int worldColumn = screenColumn - Constants.AvatarScreenColumn + _world.Avatar.Column;
                    int worldRow = screenRow - Constants.AvatarScreenRow + _world.Avatar.Row;

                    var cell = _world[worldColumn][worldRow];

                    UpdateTerrain(screenColumn, screenRow, cell.Terrain);

                    UpdateCreature(screenColumn, screenRow, cell.Creature?.CreatureType);

                    UpdateSplat(screenColumn, screenRow, cell.Creature?.Hit);

                    cell.DoHousekeeping();
                }
            }
        }

        private void UpdateSplat(int screenColumn, int screenRow, bool? hit)
        {
            var controlName = string.Format("SplatTile{0:D2}{1:D2}", screenColumn, screenRow);
            Dispatcher.Invoke(() => {
                var image = (Image)FindName(controlName);
                var imageFileName = (hit ?? false) ? ("/Assets/Images/Splat.png") : ("/Assets/Images/Blank.png");
                image.Source = new BitmapImage(new Uri(imageFileName, UriKind.RelativeOrAbsolute));
            });
        }

        private void UpdateCreature(int screenColumn, int screenRow, CreatureType? creatureType)
        {
            var controlName = string.Format("ForegroundTile{0:D2}{1:D2}", screenColumn, screenRow);
            Dispatcher.Invoke(() => {
                var image = (Image)FindName(controlName);
                image.Source = new BitmapImage(new Uri((creatureType ?? CreatureType.None).GetImageFileName(), UriKind.RelativeOrAbsolute));
            });
        }

        private void UpdateTerrain(int screenColumn, int screenRow, Terrain terrain)
        {
            string controlName = string.Format("BackgroundTile{0:D2}{1:D2}", screenColumn, screenRow);
            Dispatcher.Invoke(() => {
                Image image = (Image)FindName(controlName);
                image.Source = new BitmapImage(new Uri(terrain.GetImageFileName(), UriKind.RelativeOrAbsolute));
            });

        }

        private void UpdateStats()
        {
            var tagonCreature = _world[_world.Avatar.Column][_world.Avatar.Row].Creature as TagonCreature;
            if(tagonCreature!=null)
            {
                Dispatcher.Invoke(() =>
                {
                    ((TextBlock)FindName("attackStat")).Text = $"{tagonCreature.AttackStat}";
                    ((TextBlock)FindName("defendStat")).Text = $"{tagonCreature.DefendStat}";
                    ((TextBlock)FindName("healthStat")).Text = $"{tagonCreature.Health}/{tagonCreature.MaximumHealth}";
                    ((TextBlock)FindName("speedStat")).Text = $"{tagonCreature.SpeedStat}";
                    ((TextBlock)FindName("experienceStat")).Text = $"{tagonCreature.ExperienceStat}/{tagonCreature.ExperienceGoalStat}";
                    ((TextBlock)FindName("levelStat")).Text = $"{tagonCreature.LevelStat}";
                    ((TextBlock)FindName("goldStat")).Text = $"{tagonCreature.GoldState}";
                });
            }
        }
    }
}
