﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public enum CreatureType
    {
        None,
        Tagon,
        Rat,
        Bat,
        FloatingEye,
        Mummy,
        Necromancer,
        Shroom,
        Skeleton,
        Spider,
        Vampire,
        Zombie
    }
    public static class CreatureTypeExtensions
    {
        public static string GetImageFileName(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.None:
                    return "/Assets/Images/Blank.png";
                case CreatureType.Tagon:
                    return "/Assets/Images/Tagon.png";
                case CreatureType.Bat:
                    return "/Assets/Images/Creatures/Bat.png";
                case CreatureType.Rat:
                    return "/Assets/Images/Creatures/Rat.png";
                case CreatureType.Mummy:
                    return "/Assets/Images/Creatures/Mummy.png";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public static int GetMaximumHealth(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.Bat:
                    return 2;
                case CreatureType.Rat:
                    return 1;
                case CreatureType.Mummy:
                    return 2;
                default:
                    throw new NotImplementedException();
            }
        }
        public static Dictionary<int, int> GetAttackDice(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.Bat:
                    return Utility.GetWhiteShieldDice(2);
                case CreatureType.Rat:
                    return Utility.GetWhiteShieldDice(1);
                case CreatureType.Mummy:
                    return Utility.GetWhiteShieldDice(3);
                default:
                    throw new NotImplementedException();
            }
        }
        public static Dictionary<int, int> GetDefendDice(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.Bat:
                    return Utility.GetBlackShieldDice(2);
                case CreatureType.Rat:
                    return Utility.GetBlackShieldDice(1);
                case CreatureType.Mummy:
                    return Utility.GetBlackShieldDice(4);
                default:
                    throw new NotImplementedException();
            }
        }
        public static double GetMovementCost(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.Bat:
                    return 1.0/10;
                case CreatureType.Rat:
                    return 1.0/8;
                case CreatureType.Mummy:
                    return 1.0/4;
                default:
                    throw new NotImplementedException();
            }
        }
        public static int GetExperience(this CreatureType creatureType)
        {
            switch (creatureType)
            {
                case CreatureType.Bat:
                    return 2;
                case CreatureType.Rat:
                    return 1;
                case CreatureType.Mummy:
                    return 2;
                default:
                    throw new NotImplementedException();
            }
        }
    }
    public static class CreatureTypeUtility
    {
        private static Dictionary<CreatureType, int> _table = new Dictionary<CreatureType, int>()
        {
            [CreatureType.None] = 6000,
            [CreatureType.Rat] = 20,
            [CreatureType.Bat] = 10,
            [CreatureType.Mummy] = 5
        };
        public static CreatureType SpawnCreatureType(Random random)
        {
            return _table.Generate(random);
        }
    }
}
