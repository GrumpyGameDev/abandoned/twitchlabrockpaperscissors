﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public enum MoveDirection
    {
        North,
        East,
        South,
        West
    }
    public static class MoveDirectionExtensions
    {
        public static int GetNextColumn(this MoveDirection direction, int column)
        {
            switch(direction)
            {
                case MoveDirection.North:
                case MoveDirection.South:
                    return column;
                case MoveDirection.East:
                    return column + 1;
                case MoveDirection.West:
                    return column - 1;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public static int GetNextRow(this MoveDirection direction, int row)
        {
            switch (direction)
            {
                case MoveDirection.North:
                    return row - 1;
                case MoveDirection.South:
                    return row + 1;
                case MoveDirection.East:
                case MoveDirection.West:
                    return row;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }
    }
}
