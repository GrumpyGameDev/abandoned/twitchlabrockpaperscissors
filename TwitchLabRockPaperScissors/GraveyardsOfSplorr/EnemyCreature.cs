﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class EnemyCreature: Creature
    {
        public int Experience { get; set; }
        public Dictionary<int, int> AttackDice { get; set; }
        public Dictionary<int, int> DefendDice { get; set; }
        public EnemyCreature(CreatureType creatureType)
            :base(creatureType)
        {
            MaximumHealth = creatureType.GetMaximumHealth();
            Health = creatureType.GetMaximumHealth();
            MovementCost = creatureType.GetMovementCost();
            AttackDice = creatureType.GetAttackDice();
            DefendDice = creatureType.GetDefendDice();
            Experience = creatureType.GetExperience();
        }

        public override int RollAttack(Random random)
        {
            return AttackDice.Generate(random);
        }

        public override int RollDefend(Random random)
        {
            return DefendDice.Generate(random);
        }

        public override MoveDirection? ChooseMoveDirection()
        {
            Dictionary<MoveDirection, int> moves = new Dictionary<MoveDirection, int>();
            if (WorldCell.Row < World.Avatar.Row)
            {
                moves[MoveDirection.North] = 1;
                moves[MoveDirection.South] = 10;
            }
            else if (WorldCell.Row > World.Avatar.Row)
            {
                moves[MoveDirection.North] = 10;
                moves[MoveDirection.South] = 1;
            }
            else
            {
                moves[MoveDirection.North] = 1;
                moves[MoveDirection.South] = 1;
            }
            if (WorldColumn.Column < World.Avatar.Column)
            {
                moves[MoveDirection.West] = 1;
                moves[MoveDirection.East] = 10;
            }
            else if (WorldColumn.Column > World.Avatar.Column)
            {
                moves[MoveDirection.West] = 10;
                moves[MoveDirection.East] = 1;
            }
            else
            {
                moves[MoveDirection.East] = 1;
                moves[MoveDirection.West] = 1;
            }
            return moves.Generate(World.Random);
        }
    }
}
