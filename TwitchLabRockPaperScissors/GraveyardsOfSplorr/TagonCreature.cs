﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class TagonCreature: Creature
    {
        public int AttackStat { get; internal set; }
        public int DefendStat { get; internal set; }
        public int SpeedStat { get; internal set; }
        public int ExperienceStat { get; internal set; }
        public int ExperienceGoalStat { get; internal set; }
        public int LevelStat { get; internal set; }
        public int GoldState { get; internal set; }

        public TagonCreature()
            :base(CreatureType.Tagon)
        {
            MovementCost = 1.0/7;
            MovementCounter = 0;
            AttackStat = 3;
            DefendStat = 2;
            Health = 5;
            MaximumHealth = 5;
            SpeedStat = 7;
            ExperienceStat = 0;
            ExperienceGoalStat = 10;
            LevelStat = 1;
            GoldState = 0;
        }

        public override int RollAttack(Random random)
        {
            return Utility.GetSkullDice(AttackStat).Generate(random);
        }

        public override int RollDefend(Random random)
        {
            return Utility.GetWhiteShieldDice(AttackStat).Generate(random);
        }

        public void AddExperience(int experience)
        {
            ExperienceStat += experience;
            if(ExperienceStat>=ExperienceGoalStat)
            {
                LevelStat++;
                MaximumHealth++;
                Health = MaximumHealth;
                ExperienceStat -= ExperienceGoalStat;
                ExperienceGoalStat *= 2;
            }
        }

        public override MoveDirection? ChooseMoveDirection()
        {
            return null;
        }
    }
}
