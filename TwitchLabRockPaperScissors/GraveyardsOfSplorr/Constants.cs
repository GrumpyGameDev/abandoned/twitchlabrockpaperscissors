﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public static class Constants
    {
        public const int WorldColumns = 256;
        public const int WorldRows = 256;
        public const int ScreenColumns = 17;
        public const int ScreenRows = 15;
        public const int AvatarScreenColumn = 8;
        public const int AvatarScreenRow = 7;
        public const int TimeRadius = 20;
    }
}
