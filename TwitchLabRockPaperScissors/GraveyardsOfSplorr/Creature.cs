﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public abstract class Creature
    {
        public CreatureType CreatureType { get; }
        public WorldCell WorldCell { get; set; }
        public WorldColumn WorldColumn => WorldCell?.WorldColumn;
        public World World => WorldColumn?.World;

        public Creature(CreatureType creatureType)
        {
            CreatureType = creatureType;
        }
        public int Health { get; set; }
        public int MaximumHealth { get; set; }
        public double MovementCounter { get; set; }
        public double MovementCost { get; set; }
        public bool Hit { get; set; }
        public abstract int RollAttack(Random random);
        public abstract int RollDefend(Random random);
        public abstract MoveDirection? ChooseMoveDirection();
    }
}
