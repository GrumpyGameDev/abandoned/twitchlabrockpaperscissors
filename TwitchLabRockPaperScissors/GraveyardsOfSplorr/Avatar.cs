﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class Avatar
    {
        public World World { get; }
        public int Column { get; set; }
        public int Row { get; set; }
        public Avatar(World world)
        {
            World = world;
            Column = Constants.WorldColumns / 2;
            Row = Constants.WorldRows / 2;
        }
    }
}
