﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public enum Terrain
    {
        Ground,
        Ground2
    }
    public static class TerrainUtility
    {
        private static Dictionary<Terrain, int> _terrainTable = new Dictionary<Terrain, int>()
        {
            [Terrain.Ground] = 40,
            [Terrain.Ground2] = 1
        };
        public static Terrain SpawnTerrain(Random random)
        {
            return _terrainTable.Generate(random);
        }
        public static string GetImageFileName(this Terrain terrain)
        {
            switch (terrain)
            {
                case Terrain.Ground:
                    return "/Assets/Images/Ground.png";
                case Terrain.Ground2:
                    return "/Assets/Images/Ground2.png";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
