﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class World
    {
        public Random Random { get; }
        private Dictionary<int, WorldColumn> _worldColumns = new Dictionary<int, WorldColumn>();
        public Avatar Avatar { get; private set; }
        public bool AvatarDead
        {
            get
            {
                return this[Avatar.Column][Avatar.Row].Creature == null;
            }
        }

        public WorldColumn this[int index]
        {
            get
            {
                index = Utility.FloorModulus(index, Constants.WorldColumns);
                WorldColumn result = null;
                if(!_worldColumns.TryGetValue(index, out result))
                {
                    result = new WorldColumn(this, index);
                    _worldColumns[index] = result;
                }
                return result;
            }
        }
        public World()
        {
            Random = new Random();
            Avatar = new Avatar(this) { Column = 0, Row = 0 };
            this[Avatar.Column][Avatar.Row].Creature = new TagonCreature();
        }

        public void DoAttack(Creature creatureFrom, Creature creatureTo)
        {
            int damage = creatureFrom.RollAttack(Random) - creatureTo.RollDefend(Random);
            if(damage>0)
            {
                creatureTo.Hit = true;
                creatureTo.Health -= damage;
            }
            else
            {

            }
        }

        internal void MoveAvatar(MoveDirection direction)
        {
            int nextColumn = direction.GetNextColumn(Avatar.Column);
            int nextRow = direction.GetNextRow(Avatar.Row);

            var cell = this[Avatar.Column][Avatar.Row];
            var tagon = cell.Creature as TagonCreature;
            if(tagon!=null)
            {
                cell.Creature = null;

                var nextCell = this[nextColumn][nextRow];
                if (nextCell.Creature != null)
                {
                    DoAttack(tagon, nextCell.Creature);
                    if(nextCell.Creature.Health<=0)
                    {
                        tagon.AddExperience((nextCell.Creature as EnemyCreature).Experience);
                    }
                    nextColumn = Avatar.Column;
                    nextRow = Avatar.Row;
                    ApplyMovementToTimeRadius(nextColumn, nextRow, 1);
                }
                else
                {
                    ApplyMovementToTimeRadius(nextColumn, nextRow, tagon.MovementCost);
                }

                Avatar.Column = nextColumn;
                Avatar.Row = nextRow;
                cell = this[Avatar.Column][Avatar.Row];
                cell.Creature = tagon;

                UpdateTimeRadius(Avatar.Column, Avatar.Row);
            }
        }

        private void UpdateTimeRadius(int centerColumn, int centerRow)
        {
            Dictionary<Creature, (int, int)> positions = new Dictionary<Creature, (int, int)>();
            for (int column = centerColumn - Constants.TimeRadius; column <= centerColumn + Constants.TimeRadius; ++column)
            {
                for (int row = centerRow - Constants.TimeRadius; row <= centerRow + Constants.TimeRadius; ++row)
                {
                    var creature = this[column][row].Creature;
                    if(creature!=null && creature.CreatureType!= CreatureType.Tagon && creature.MovementCounter>0)
                    {
                        positions[creature] = (column, row);
                    }
                }
            }
            foreach (var entry in positions)
            {
                var creature = entry.Key;
                while(creature!=null && creature.MovementCounter>0)
                {
                    var cell = this[entry.Value.Item1][entry.Value.Item2];
                    if (cell.Creature == creature)
                    {
                        if (creature.Health <= 0)
                        {
                            creature = null;
                        }
                        else
                        {
                            var direction = creature.ChooseMoveDirection();
                            if(direction.HasValue)
                            {
                                int nextColumn = direction.Value.GetNextColumn(entry.Value.Item1);
                                int nextRow = direction.Value.GetNextRow(entry.Value.Item2);
                                var nextCell = this[nextColumn][nextRow];
                                if (nextCell.Creature != null)
                                {
                                    if (nextCell.Creature.CreatureType == CreatureType.Tagon)
                                    {
                                        DoAttack(creature, nextCell.Creature);
                                        creature.MovementCounter -= 1;
                                    }
                                    else
                                    {
                                        creature.MovementCounter -= creature.MovementCost;
                                    }
                                }
                                else
                                {
                                    cell.Creature = null;
                                    nextCell.Creature = creature;
                                    creature.MovementCounter -= creature.MovementCost;
                                }
                            }
                        }
                    }
                    else
                    {
                        creature = null;
                    }
                }
            }
        }

        private void ApplyMovementToTimeRadius(int centerColumn, int centerRow, double movement)
        {
            for(int column = centerColumn - Constants.TimeRadius;column<=centerColumn+Constants.TimeRadius;++column)
            {
                for (int row = centerRow - Constants.TimeRadius; row <= centerRow + Constants.TimeRadius; ++row)
                {
                    var creature = this[column][row].Creature;
                    if(creature!=null)
                    {
                        creature.MovementCounter += movement;
                    }
                }
            }
        }
    }
}
