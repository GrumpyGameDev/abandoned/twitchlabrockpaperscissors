﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class WorldColumn
    {
        public World World { get; }
        public int Column { get; private set; }
        private Dictionary<int, WorldCell> _worldCells = new Dictionary<int, WorldCell>();
        public WorldColumn(World world, int column)
        {
            World = world;
            Column = column;
        }
        public WorldCell this[int index]
        {
            get
            {
                index = Utility.FloorModulus(index, Constants.WorldRows);
                WorldCell result = null;
                if (!_worldCells.TryGetValue(index, out result))
                {
                    result = new WorldCell(this, index);
                    _worldCells[index] = result;
                }
                return result;
            }
        }
    }
}
