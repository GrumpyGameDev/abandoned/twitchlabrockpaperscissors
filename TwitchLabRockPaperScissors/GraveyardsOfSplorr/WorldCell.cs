﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public class WorldCell
    {
        public Terrain Terrain { get; set; }
        public WorldColumn WorldColumn { get; }
        public int Row { get; private set; }
        private Creature _creature = null;
        public Creature Creature
        {
            get
            {
                return _creature;
            }
            set
            {
                if(_creature!=null)
                {
                    _creature.WorldCell = null;
                }
                _creature = value;
                if(_creature!=null)
                {
                    _creature.WorldCell = this;
                }
            }
        }
        public World World => WorldColumn?.World;
        public WorldCell(WorldColumn worldColumn, int row)
        {
            WorldColumn = worldColumn;
            Row = row;
            Terrain = TerrainUtility.SpawnTerrain(World?.Random);
            var creatureType = CreatureTypeUtility.SpawnCreatureType(World?.Random);
            if(creatureType!= CreatureType.None)
            {
                Creature = new EnemyCreature(creatureType);
            }
        }

        public void DoHousekeeping()
        {
            if (Creature?.Hit ?? false)
            {
                Creature.Hit = false;
                if (Creature.Health <= 0)
                {
                    Creature = null;
                }
            }
        }
    }
}
