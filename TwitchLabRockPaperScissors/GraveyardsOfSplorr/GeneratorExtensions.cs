﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraveyardsOfSplorr
{
    public static class GeneratorExtensions
    {
        public static Dictionary<int, int> Combine(this Dictionary<int, int> table, Dictionary<int, int> otherTable)
        {
            return Combine(table, otherTable, (a, b) => a + b);
        }
        public static Dictionary<T,int> Combine<T>(this Dictionary<T, int> table, Dictionary<T, int> otherTable, Func<T,T,T> combiner)
        {
            Dictionary<T, int> result = new Dictionary<T, int>();
            foreach(var entry in table)
            {
                foreach(var otherEntry in otherTable)
                {
                    var combined = combiner(entry.Key, otherEntry.Key);
                    var weight = entry.Value * otherEntry.Value;
                    if(result.ContainsKey(combined))
                    {
                        result[combined] += weight;
                    }
                    else
                    {
                        result.Add(combined, weight);
                    }
                }
            }
            return result;
        }
        public static T Generate<T>(this Dictionary<T, int> table, Random random)
        {
            var total = table.Values.Sum();
            var generated = random.Next(total);
            foreach (var entry in table)
            {
                generated -= entry.Value;
                if (generated < 0)
                {
                    return entry.Key;
                }
            }
            throw new NotImplementedException();
        }
    }
}
