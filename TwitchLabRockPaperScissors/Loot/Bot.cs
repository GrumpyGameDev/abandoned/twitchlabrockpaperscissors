﻿using System;
using System.Configuration;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;

namespace Loot
{
    class Bot
    {
        TwitchClient client;

        public Bot()
        {
            ConnectionCredentials credentials = new ConnectionCredentials(ConfigurationManager.AppSettings["twitch_username"], ConfigurationManager.AppSettings["access_token"]);

            client = new TwitchClient();
            client.Initialize(credentials, ConfigurationManager.AppSettings["channel"]);

            client.OnConnected += OnConnected;
            client.OnJoinedChannel += OnJoinedChannel;
            client.OnMessageReceived += OnMessageReceived;
            client.OnWhisperReceived += OnWhisperReceived;
            client.OnNewSubscriber += OnNewSubscriber;


            client.Connect();
        }

        private void OnConnected(object sender, OnConnectedArgs e)
        {
        }
        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
        }

        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            var tokens = e.ChatMessage.Message.Split(' ');
            foreach (var token in tokens)
            {
                Game.HandleCommand(e.ChatMessage.Username, token);
            }
            Game.UpdateDisplay();
        }

        private void OnWhisperReceived(object sender, OnWhisperReceivedArgs e)
        {
        }

        private void OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
        }
    }
}