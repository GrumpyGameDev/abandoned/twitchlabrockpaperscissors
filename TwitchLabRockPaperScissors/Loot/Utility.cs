﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loot
{
    public static class Utility
    {
        private static Random random = new Random();
        public static int GenerateFromRange(int min, int max)
        {
            return random.Next(min, max + 1);
        }
    }
}
