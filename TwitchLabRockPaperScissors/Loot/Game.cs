﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loot
{
    public static class Game
    {
        public static int _avatarX;
        public static int _avatarY;
        public static int _goalX;
        public static int _goalY;
        public static int _score;
        public static int _maximumX;
        public static int _maximumY;
        public static int _minimumX = 0;
        public static int _minimumY = 1;



        public static void Run()
        {
            //set up display and bounds
            Console.BufferWidth = Console.WindowWidth;
            Console.BufferHeight = Console.WindowHeight;
            _maximumX = Console.BufferWidth - 1;
            _maximumY = Console.BufferHeight - 2;
            Console.CursorVisible = false;

            SetUpAvatar();
            SetUpGoal();
            bool done = false;
            UpdateDisplay();
            while (!done)
            {
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey(true);
                    done = key.Key == ConsoleKey.Escape;
                }
            }
        }

        private static void SetUpGoal()
        {
            do
            {
                _goalX = Utility.GenerateFromRange(_minimumX, _maximumX);
                _goalY = Utility.GenerateFromRange(_minimumY, _maximumY);
            } while (_goalX == _avatarX && _goalY == _avatarY);
        }

        private static void SetUpAvatar()
        {
            _avatarX = Console.BufferWidth / 2;
            _avatarY = Console.BufferHeight / 2;
        }

        private static void PutCharacter(int x,int y,ConsoleColor bg,ConsoleColor fg,char ch)
        {
            Console.CursorLeft = x;
            Console.CursorTop = y;
            Console.BackgroundColor = bg;
            Console.ForegroundColor = fg;
            Console.Write(ch);
        }

        internal static void UpdateDisplay()
        {
            if(_goalX==_avatarX && _goalY == _avatarY)
            {
                _score++;
                SetUpGoal();
                Console.Beep();
            }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.WriteLine($"Score: {_score}");

            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.CursorLeft = 0;
            Console.CursorTop = Console.BufferHeight - 1;
            Console.Write("Commands: !up !down !left !right (multicommand with spaces in between)         ");

            PutCharacter(_goalX, _goalY, ConsoleColor.Black, ConsoleColor.Yellow, '$');
            PutCharacter(_avatarX, _avatarY, ConsoleColor.Black, ConsoleColor.White, '@');
        }

        internal static void HandleCommand(string username, string message)
        {
            message = message.ToLower();
            if (message == "!up")
            {
                if (_avatarY > _minimumY)
                {
                    _avatarY--;
                    UpdateDisplay();
                }
            }
            else if (message == "!down")
            {
                if (_avatarY < _maximumY)
                {
                    _avatarY++;
                    UpdateDisplay();
                }
            }
            else if (message == "!left")
            {
                if (_avatarX > _minimumX)
                {
                    _avatarX--;
                    UpdateDisplay();
                }

            }
            else if (message == "!right")
            {
                if (_avatarX < _maximumX)
                {
                    _avatarX++;
                    UpdateDisplay();
                }
            }
        }
    }
}
