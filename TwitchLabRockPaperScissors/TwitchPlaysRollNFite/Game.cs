﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitchPlaysRollNFite
{
    public static class Game
    {
        private static World _world = new World();

        public static void Run()
        {
            Console.Title = "Roll-N-Fite!!";
            bool done = false;
            UpdateDisplay();
            while (!done)
            {
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey(true);
                    done = key.Key == ConsoleKey.Escape;
                }
            }
        }


        internal static void UpdateDisplay()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Green;
            var messages = _world.Messages;
            if(messages.Any())
            {
                foreach(var message in messages)
                {
                    Console.WriteLine(message);
                }
                Console.WriteLine();
                _world.ClearMessages();
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("Exits: ");
            foreach(var direction in _world.CurrentBranch.ExitDirections)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write($"{direction.GetCommand()} ");
            }
        }

        internal static void HandleCommand(string username, string message)
        {
            if(message=="!back")
            {
                _world.Move(BranchDirection.Back);
            }
            else if (message == "!ahead")
            {
                _world.Move(BranchDirection.Ahead);
            }
            else if (message == "!left")
            {
                _world.Move(BranchDirection.Left);
            }
            else if (message == "!right")
            {
                _world.Move(BranchDirection.Right);
            }
        }
    }
}
