﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitchPlaysRollNFite
{
    public static class RandomExtensions
    {
        public static int RollDie(this Random random, int dieSize)
        {
            return random.Next(dieSize) + 1;
        }
        public static IEnumerable<T> Pick<T>(this Random random, IEnumerable<T> source, int count)
        {
            return source.OrderBy(x => random.Next()).Take(count);
        }
    }
}
