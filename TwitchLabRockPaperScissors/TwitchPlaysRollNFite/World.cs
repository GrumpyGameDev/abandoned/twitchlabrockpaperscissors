﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitchPlaysRollNFite
{
    public class World
    {
        private readonly Random _random = new Random();
        private List<string> _messages = new List<string>();

        public World()
        {
            CurrentBranch = new Branch(_random);
            AddMessage("Welcome to Roll-N-Fite!!");
        }
        public void ClearMessages()
        {
            _messages.Clear();
        }
        public IEnumerable<string> Messages => _messages.AsEnumerable();
        public void AddMessage(string message)
        {
            _messages.Add(message);
        }
        public Branch CurrentBranch { get; private set; }
        public void Move(BranchDirection direction)
        {
            CurrentBranch = CurrentBranch.DoMove(_random, direction, AddMessage);
        }
    }
}
