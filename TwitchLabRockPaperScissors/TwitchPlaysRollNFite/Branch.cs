﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitchPlaysRollNFite
{
    public class Branch
    {
        private static readonly IEnumerable<BranchDirection> _availableDirections = new BranchDirection[] { BranchDirection.Ahead, BranchDirection.Left, BranchDirection.Right };
        private Dictionary<BranchDirection, Branch> _exits = new Dictionary<BranchDirection, Branch>();

        public Branch(Random random)
            : this(random,null)
        {

        }
        public Branch(Random random, Branch previous)
        {
            if(previous!=null)
            {
                _exits[BranchDirection.Back] = previous;
            }

            int exitCount = random.RollDie(2) + random.RollDie(2) - 1;//2d2-1
            random.Pick(_availableDirections, exitCount).ToList().ForEach(x =>
            {
                _exits[x] = null;
            });
        }
        public IEnumerable<BranchDirection> ExitDirections => _exits.Select(x => x.Key);
        public bool HasExit(BranchDirection direction)
        {
            return _exits.ContainsKey(direction);
        }
        public bool CanMove => true;
        public Branch DoMove(Random random, BranchDirection direction, Action<string> messageAction)
        {
            if(CanMove)
            {
                if (HasExit(direction))
                {
                    messageAction($"You go {direction}.");
                    var nextBranch = _exits[direction];
                    if (nextBranch == null)
                    {
                        nextBranch = new Branch(random, this);
                        _exits[direction] = nextBranch;
                    }
                    return nextBranch;
                }
                else
                {
                    messageAction("You cannot go that way!");
                    return this;
                }
            }
            else
            {
                messageAction("You cannot move at this time!");
                return this;
            }
        }

    }
}
