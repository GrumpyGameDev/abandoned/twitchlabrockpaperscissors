﻿using System;

namespace TwitchPlaysRollNFite
{
    public enum BranchDirection
    {
        Back,
        Left,
        Ahead,
        Right
    }
    public static class BranchDirectionExtensions
    {
        public static string GetCommand(this BranchDirection direction)
        {
            switch(direction)
            {
                case BranchDirection.Ahead:
                    return "!ahead";
                case BranchDirection.Back:
                    return "!back";
                case BranchDirection.Left:
                    return "!left";
                case BranchDirection.Right:
                    return "!right";
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
