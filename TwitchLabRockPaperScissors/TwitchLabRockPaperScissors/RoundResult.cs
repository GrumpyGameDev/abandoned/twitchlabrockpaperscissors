﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchLabRockPaperScissors
{
    public enum RoundResult
    {
        None,
        TwitchWins,
        ComputerWins,
        TwitchAndComputerTie,
        TieVote
    }
}
