﻿using System;
using System.Configuration;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;

namespace TwitchLabRockPaperScissors
{
    class Program
    {
        static void Main(string[] args)
        {
            Bot bot = new Bot();
            Game.Run();
        }
    }
}