﻿using System;
using System.Configuration;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;

namespace TwitchLabRockPaperScissors
{
    class Bot
    {
        TwitchClient client;

        public Bot()
        {
            ConnectionCredentials credentials = new ConnectionCredentials(ConfigurationManager.AppSettings["twitch_username"], ConfigurationManager.AppSettings["access_token"]);

            client = new TwitchClient();
            client.Initialize(credentials, ConfigurationManager.AppSettings["channel"]);

            client.OnConnected += OnConnected;
            client.OnJoinedChannel += OnJoinedChannel;
            client.OnMessageReceived += OnMessageReceived;
            client.OnWhisperReceived += OnWhisperReceived;
            client.OnNewSubscriber += OnNewSubscriber;


            client.Connect();
        }

        private void OnConnected(object sender, OnConnectedArgs e)
        {
        }
        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
        }

        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            Game.HandleVote(e.ChatMessage.Username, e.ChatMessage.Message);
            Game.UpdateDisplay();
        }

        private void OnWhisperReceived(object sender, OnWhisperReceivedArgs e)
        {
        }

        private void OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
        }
    }
}