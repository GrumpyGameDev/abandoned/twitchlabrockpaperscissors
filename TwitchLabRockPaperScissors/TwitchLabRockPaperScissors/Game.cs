﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchLabRockPaperScissors
{
    public static class Game
    {
        private const int MsPerRound = 30000;
        private static DateTimeOffset _endOfRound;
        private static Dictionary<string, Thingie> _votes = new Dictionary<string, Thingie>();
        private static RoundResult _lastResult = RoundResult.None;
        private static string _lastRoundMessage = "No previous round.";
        private static Dictionary<Winner, int> _scores = new Dictionary<Winner, int>()
        {
            [Winner.Twitch]=0,
            [Winner.Computer]=0,
            [Winner.Ties] = 0,
        };

        internal static void Run()
        {
            bool done = false;
            _endOfRound = DateTimeOffset.Now.AddMilliseconds(MsPerRound);
            UpdateDisplay();
            while (!done)
            {
                if(Console.KeyAvailable)
                {
                    var key = Console.ReadKey(true);
                    done = key.Key == ConsoleKey.Escape;
                }
                if(DateTimeOffset.Now>= _endOfRound)
                {
                    CompleteRound();
                    _endOfRound = DateTimeOffset.Now.AddMilliseconds(MsPerRound);
                    UpdateDisplay();
                }
            }
        }

        private static void CompleteRound()
        {
            var tallies = GetVoteTallies();
            var lead = tallies.Max(x => x.Value);
            if(lead==0)
            {
                _lastRoundMessage = "No votes! Throw it out!";
                _lastResult = RoundResult.None;
            }
            else
            {
                var voteWinners = tallies.Where(x => x.Value == lead).Select(x=>x.Key);
                if (voteWinners.Count() > 1)
                {
                    _lastRoundMessage = "Tied votes! Throw it out!";
                    _lastResult = RoundResult.TieVote;
                }
                else
                {
                    var twitchThingie = voteWinners.Single();
                    var roll = new Random().Next(3);
                    var computerThingie =
                        (roll == 0) ? (Thingie.Rock) :
                        (roll == 1) ? (Thingie.Scissors) :
                        (Thingie.Paper);
                    var winner = EvaluateWinner(twitchThingie, computerThingie);
                    _scores[winner]++;
                    switch(winner)
                    {
                        case Winner.Computer:
                            _lastResult = RoundResult.ComputerWins;
                            break;
                        case Winner.Ties:
                            _lastResult = RoundResult.TwitchAndComputerTie;
                            break;
                        case Winner.Twitch:
                            _lastResult = RoundResult.TwitchWins;
                            break;
                    }
                }
            }
            _votes.Clear();
        }

        private static Winner EvaluateWinner(Thingie twitchThingie, Thingie computerThingie)
        {
            _lastRoundMessage = $"Last Round: Twitch plays {twitchThingie}, Computer plays {computerThingie}";
            switch(twitchThingie)
            {
                case Thingie.Paper:
                    switch(computerThingie)
                    {
                        case Thingie.Paper:
                            return Winner.Ties;
                        case Thingie.Rock:
                            return Winner.Twitch;
                        case Thingie.Scissors:
                            return Winner.Computer;
                        default:
                            throw new NotImplementedException();
                    }
                case Thingie.Rock:
                    switch (computerThingie)
                    {
                        case Thingie.Paper:
                            return Winner.Computer;
                        case Thingie.Rock:
                            return Winner.Ties;
                        case Thingie.Scissors:
                            return Winner.Twitch;
                        default:
                            throw new NotImplementedException();
                    }
                case Thingie.Scissors:
                    switch (computerThingie)
                    {
                        case Thingie.Paper:
                            return Winner.Twitch;
                        case Thingie.Rock:
                            return Winner.Computer;
                        case Thingie.Scissors:
                            return Winner.Ties;
                        default:
                            throw new NotImplementedException();
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        public static void HandleVote(string userName, string vote)
        {
            if(vote.ToLower()=="!rock")
            {
                _votes[userName] = Thingie.Rock;
            }
            else if (vote.ToLower() == "!scissors")
            {
                _votes[userName] = Thingie.Scissors;
            }
            else if (vote.ToLower() == "!paper")
            {
                _votes[userName] = Thingie.Paper;
            }
        }

        internal static void UpdateDisplay()
        {
            Console.Clear();
            ShowLastRoundResult();
            ShowEndOfRound();
            ShowScores();
            ShowCurrentRoundVotes();
            ShowInstructions();
        }

        private static void ShowInstructions()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("How to play:\nEach round lasts 30 seconds.\nVote with !rock, !paper, !scissors.\nMost popular play is made at end of round.");
        }

        private static void ShowLastRoundResult()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(_lastRoundMessage);
            Console.WriteLine($"Status: {_lastResult}");
            Console.WriteLine();
        }

        private static void ShowEndOfRound()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Rounds Ends: {_endOfRound.ToString()}");
            Console.WriteLine();
        }

        private static void ShowScores()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("==============Scores==============");
            var lead = _scores.Max(x => x.Value);
            foreach (var entry in _scores)
            {
                if (entry.Value == lead)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine($"{entry.Key} - {entry.Value}");
            }
            Console.WriteLine();
        }

        private static Dictionary<Thingie,int> GetVoteTallies()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("==========Current  Round==========");
            Dictionary<Thingie, int> tallies = new Dictionary<Thingie, int>()
            {
                [Thingie.Rock] = 0,
                [Thingie.Scissors] = 0,
                [Thingie.Paper] = 0
            };
            foreach (var entry in _votes)
            {
                tallies[entry.Value]++;
            }
            return tallies;
        }

        private static void ShowCurrentRoundVotes()
        {
            var tallies = GetVoteTallies();
            var lead = tallies.Max(x => x.Value);
            foreach (var entry in tallies)
            {
                if (entry.Value == lead)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine($"{entry.Key} - {entry.Value} votes");
            }
        }
    }
}
